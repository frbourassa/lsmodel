# Antigen encoding latent space model
Code to compute latent space trajectories $LS1(t)$, $LS2(t)$ (time integrals) and $ls1(t)$, $ls2(t)$ (concentrations)
based on model parameter fits and interpolation as a function of antigen EC50 from the antigen encoding projet.

This is mostly repackaging in a more user-friendly way code from the [antigen encoding theory repository](https://github.com/frbourassa/antigen_encoding_theory/).

## Usage
The purpose of this package is to easily generate LS curves given time points and a peptide EC50. Parameter distributions are estimated and interpolated as a function of EC50 with helper functions, and then interpolated distributions are passed as arguments to the functions generating trajectories.



Steps to follow when using `lsmodel`:
  1. Put the lsmodel/ directory in the main folder of your code project
  2. In your scripts using the package:
    - 2.1 `import lsmodel as lsm`;
    - 2.2 Load existing parameter fits on actual data and antigen EC50s (provided with the package, load with function `lsm.load_param_fits`);
    - 2.3 Estimate and interpolate parameter distributions as a function of $\log_{10}$ EC50 (with the `lsm.interpolate_param_distrib` function);
    - 2.4 Generate LS curves at the times and $\log_{10}$ EC50s of your choice based on these interpolated distributions, using the functions:
      - `lsm.generate_LS` (time integral time series in latent space)
      - `lsm.generate_ls_conc` (concentration time series in latent space)
      NB: Trajectories for a given EC50 can be generated in batch with optional argument `nsamp`; useful when when one wants many sample trajectories.
    - 2.5 It is also possible to generate model parameter samples, without computing the corresponding latent space trajectories, with the function `lsm.generate_sample_params`. This can be useful for, e.g., channel capacity calculation.

## Future improvements
 - Turn `example.py` into a Jupyter notebook.
 - Add cytokine reconstruction at some point, so we also have a user-friendly interface to generate synthetic cytokine data, and not just latent space trajectories.

[//]: # (Should be easy, I just have to import reconstruction coefficients and wrap the generate_LS and generate_ls_conc functions into a generate_cytokines function.)

## Authors and acknowledgment
Code author: François X. P. Bourassa

Project authors: Sooraj R. Achar#, François X. P. Bourassa#, Thomas J. Rademaker#, Angela Lee, Taisuke Kondo, Emanuel Salazar-Cavazos, John S. Davies, Naomi Taylor, Paul François, Grégoire Altan-Bonnet

Publication: Sooraj R. Achar<sup>#</sup>, François X. P. Bourassa<sup>#</sup>, Thomas J. Rademaker<sup>#</sup>, Angela Lee, Taisuke Kondo, Emanuel Salazar-Cavazos, John S. Davies, Naomi Taylor, Paul François, and Grégoire Altan-Bonnet. _"Universal antigen encoding of T cell activation from high dimensional cytokine data"_,
*Science*, May 2022. _(<sup>#</sup>: these authors contributed equally)_
https://www.science.org/doi/10.1126/science.abl5311

## License
GNU GPLv3
