"""
Example illustrating usage of the LS model by generating:
  1. Sample latent space curves for OT-1 peptide qualities, with variability
    for each EC50 estimated from actual parameter fits.
  2. A hundred latent space curves with evenly spaced antigen EC50s, without
    variability (average LS curve for each EC50).

Steps to follow when using the package:
  1. Import the package as `lsm`.
  2. Load existing parameter fits and EC50s
  3. Estimate and interpolate parameter distributions as a function of EC50
  4. Generate LS curves at the times and EC50s of your choice based on these
    interpolated distributions, using the functions
        lsmodel.generate_LS       (time integral time series in latent space)
        lsmodel.generate_ls_conc  (concentration time series in latent space)

@author: frbourassa
May 5, 2023
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import warnings

### 1. Import the package
import lsmodel as lsm

if __name__ == "__main__":
    ### 2. Load existing parameter fits and EC50s
    p_fits = lsm.load_param_fits()  # Use default parameter fit files

    ### 3. Estimate and interpolate parameter distributions vs log10 EC50
    p_interps = lsm.interpolate_param_distrib(p_fits)

    # Check the log10 EC50s of reference peptides used for the interpolation
    print("Log10 EC50s:\n" + str(p_interps["log10ec50s"]))

    ### 4. Use interpolated parameter distributions to generate trajectories
    # Optional: pass a seeded random number generator for reproducibility
    rgen = np.random.default_rng(0x19014146d0194973cdf2065ebf6f46a0)
    # Time ranges from 0 to 72h, typically.
    tpoints = np.arange(0.0, 72.1, 0.5)

    # With noise on the parameter values, as estimated from the original fits
    try_log10ec50 = 2.0  # 100x less potent that N4
    y = lsm.generate_LS(tpoints, try_log10ec50, p_interps,
                    add_noise=True, rgen=rgen, nsamp=1)
    # Note: the return is indexed [time, LS axis]

    # Average trajectory for a given EC50.
    y = lsm.generate_LS(tpoints, try_log10ec50, p_interps, add_noise=False)
    # Corresponding concentration trajectories
    y = lsm.generate_ls_conc(tpoints, try_log10ec50, p_interps,
                            add_noise=False, nsamp=1)


    ### Example 1: sample latent space trajectories for OT-1 EC50s with
    # noise, estimated from the distributions
    nsamples = 10
    ag_groups = p_interps["log10ec50s"]
    # Store trajectories in a dataframe indexed [Ag, Sample, Time]
    # and with columns for LS1 and LS2
    df_traj = pd.DataFrame(
            np.zeros([nsamples*ag_groups.shape[0]*tpoints.shape[0], 2]),
            index=pd.MultiIndex.from_product(
                    [ag_groups.index, np.arange(nsamples), tpoints],
                    names=["Antigen", "Sample", "Time"]),
            columns=pd.Index(["LS1", "LS2"], name="Feature")
        )
    df_traj = df_traj.sort_index()

    # Generate each trajectory, with all time points in one vectorized call
    for ag in ag_groups.index:
        ec = ag_groups.get(ag)
        y = lsm.generate_LS(tpoints, ec, p_interps,
                        add_noise=True, rgen=rgen, nsamp=nsamples)
        y = y.reindex(df_traj.loc[ag].index)
        df_traj.loc[ag,:] = y.values

    # Plot the generated trajectories
    palette = sns.color_palette(n_colors=len(ag_groups.index))
    palette = {ag:palette[i] for i, ag in enumerate(ag_groups.index)}
    fig, ax = plt.subplots()
    fig.set_size_inches(4.5, 4.)
    for ag in ag_groups.index:
        for i in range(nsamples):
            lbl = ag if i == 0 else None
            ax.plot(df_traj.loc[(ag, i), "LS1"], df_traj.loc[(ag, i), "LS2"],
                color=palette[ag], label=lbl)
    ax.set(xlabel="$LS_1$ (a.u.)", ylabel="$LS_2$ (a.u.)", xticks=[], yticks=[])
    for side in ["top", "right"]:
        ax.spines[side].set_visible(False)
    ax.legend(title="Antigen", frameon=False)
    fig.tight_layout()
    plt.show()
    plt.close()


    ### Example 2: trajectories for a closely sampled ec50 axis
    ag_axis = np.linspace(p_interps["log10ec50s"].min(),
                          p_interps["log10ec50s"].max(), 101)[::-1]
    # Store trajectories in a dataframe indexed [Ag, Sample, Time]
    # and with columns for LS1 and LS2
    df_traj2 = pd.DataFrame(
            np.zeros([ag_axis.shape[0]*tpoints.shape[0], 2]),
            index=pd.MultiIndex.from_product([ag_axis, tpoints],
                    names=["EC50", "Time"]),
            columns=pd.Index(["LS1", "LS2"], name="Feature")
        )
    df_traj2 = df_traj2.sort_index()

    # Generate each trajectory, with all time points in one vectorized call
    for ag in ag_axis:
        y = lsm.generate_LS(tpoints, ag, p_interps, add_noise=False, nsamp=1)
        y = y.reindex(df_traj2.loc[ag].index)
        df_traj2.loc[ag,:] = y.values

    # Plot the generated trajectories
    palette2 = sns.color_palette("viridis", n_colors=len(ag_axis))
    palette2 = {ag:palette2[i] for i, ag in enumerate(ag_axis)}
    fig, ax = plt.subplots()
    for ag in ag_axis:
        ax.plot(df_traj2.loc[ag, "LS1"], df_traj2.loc[ag, "LS2"],
            color=palette2[ag])
    ax.set(xlabel="$LS_1$", ylabel="$LS_2$")
    fig.tight_layout()
    plt.show()
    plt.close()
