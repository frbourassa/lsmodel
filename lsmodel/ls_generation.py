"""
Module with main user functions to generate LS or ls curves.

@author: frbourassa
May 5, 2023
"""
import numpy as np
import pandas as pd
import json
import os

from lsmodel.src.statistics import build_symmetric
from lsmodel.src.distrib_interpolation import (
    stats_per_levels,
    interpolate_params_vs_logec50,
    linear_interpolate_params_vs_logec50,
    compute_cholesky_dataframe
)
from lsmodel.src.sigmoid import (
    ballistic_sigmoid_freealpha,
    sigmoid_conc_full_freealpha
)


# NB: to make this package even easier to use, we could estimate and
# interpolate parameter distributions here at import time.
# The function interpolate_param_distrib should then be moved to src/ since it
# would not even be necessary for the user; instead, the ls_generation module
# would create a module-scope variable containing interpolated distributions,
# and the functions generate_LS below would use that variable -- no longer
# necessary as an argument to the function either.

def generate_sample_params(log10ec50, interps, rgen=None, nsamp=1, epsil=1e-5):
    """ Function that generates nsamp parameter samples for the chosen EC50,
    based on the distribution interpolations in interps.

    Args:
        log10ec50 (float): log10 of antigen EC50 (relative to best agonist)
        interps (dict): as returned by lsmodel.interpolate_param_distrib
            "ser_splines_means": ser_splines_means,
            "ser_splines_chol": ser_splines_chol,
            "ser_splines_covs": ser_splines_covs,
            "v2v1_stats": v2v1_stats,
            "tscale":paramfits.get("tscale"),
            "params_with_cov": params_with_cov,
            "secondary_params": secondary_params,
            "log10ec50s": ser_ec50s_avglog
        rgen (np.random.Generator): random number generator
        nsamp (int): number of parameter samples to generate
        epsil (float): small positive value at which standard deviation is
            clipped to enforce positive definiteness

    Returns:
        df_p (pd.DataFrame): dataframe of parameters, with an index for
            samples and columns for each parameter (including v2/v1 ratio)
    """
    if rgen is None:
        rgen = np.random.default_rng()
    all_param_names = (list(interps["params_with_cov"])
                        + list(interps["secondary_params"])
                        + ["v2v1_ratio"])
    df_p = pd.DataFrame(np.zeros([nsamp, len(all_param_names)]),
                index=pd.RangeIndex(0, nsamp, dtype=int, name="Sample"),
                columns=pd.Index(all_param_names, name="Parameter"))
    # Treat parameters that require a covariance matrix to be built from
    # the interpolation of each Cholesky matrix element
    # Then, generate nsamp samples for each key in the index.
    chol_elems = []
    mean_vec = []
    chol_interps = interps["ser_splines_chol"]
    mean_interps = interps["ser_splines_means"]
    p_with = interps["params_with_cov"]
    # Cholesky contains lower triangular elements, so i >= j
    for i, pi in enumerate(p_with):
        mean_vec.append(mean_interps.loc[pi](log10ec50))
        for j, pj in enumerate(p_with[:i+1]):
            chol_spl = chol_interps.loc[pi+"*"+pj]
            # Enforce positive definiteness by clipping diagonal elements
            # to some small positive value epsil.
            if i == j:
                chol_elems.append(max(epsil, chol_spl(log10ec50)))
            else:
                chol_elems.append(chol_spl(log10ec50))
    # Put Cholesky matrix elements in the lower triangular part of a 2D matrix
    chol = np.zeros([len(p_with), len(p_with)])
    lower_indices = np.tril_indices(len(p_with))
    chol[lower_indices] = chol_elems
    # The covariance matrix is chol @ chol.T
    cov_mat = np.dot(chol, chol.T)
    mean_vec = np.asarray(mean_vec)
    # Output shape of multivariate_normal is nsamp x ndims
    # So each sample goes in a separate row
    df_p.loc[:, p_with] = rgen.multivariate_normal(mean_vec, cov_mat, nsamp)

    # Now treat parameters that have no covariance, just separate variances
    # There is one vector-valued interpolation function for all these params
    other_p = interps["secondary_params"]
    vari_interps = interps["ser_splines_covs"]
    secondary_means = mean_interps.loc["secondary_params"](log10ec50)
    secondary_stds = np.sqrt(vari_interps.loc["secondary_params"](log10ec50))
    # Reduce the noise on these parameters, otherwise it is exaggerated
    # since distributions are usually bimodal, not gaussian at all. 
    secondary_stds = 0.2 * secondary_stds
    # Generate samples, broadcast extends means and std vectors across rows
    secondary_samples = (secondary_means
                    + secondary_stds*rgen.normal(size=[nsamp, len(other_p)]))
    df_p.loc[:, other_p] = secondary_samples

    # Add median v2/v1 ratio, no noise on that parameter after all,
    # so that all samples seem to come from the same experiment.
    med, mad = interps["v2v1_stats"].loc[["median", "MAD"]]
    df_p.loc[:, "v2v1_ratio"] = med

    # At the end, clip parameter a0, tau0, v1, alpha, beta to be >= 0
    # Clip alpha and beta to tscale/100 = 1/5
    params_clips = {"a0":(0, np.inf), "tau0":(0, np.inf), "t0":(0, np.inf),
                   "v1":(0, np.inf), "alpha":(.2, np.inf), "beta":(.2, np.inf),
                   "v2v1_ratio":(.0, 12.0)}
    for p, bounds in params_clips.items():
        try:
            df_p[p].clip(*bounds, inplace=True)
        except KeyError: continue
    return df_p


def generate_mean_params(log10ec50, interps):
    """ Just interpolate the mean of each parameter at the desired EC50.
    Analogous to generate_sample_params, but much simpler, since it
    generates only a Series (one value for each parameter), without noise.

    Args:
        log10ec50 (float): log10 of antigen EC50 (relative to best agonist)
        interps (dict): as returned by lsmodel.interpolate_param_distrib
            "ser_splines_means": ser_splines_means,
            "ser_splines_chol": ser_splines_chol,
            "ser_splines_covs": ser_splines_covs,
            "v2v1_stats": v2v1_stats,
            "tscale":paramfits.get("tscale"),
            "params_with_cov": params_with_cov,
            "secondary_params": secondary_params,
            "log10ec50s": ser_ec50s_avglog

    Returns:
        ser_p (pd.Series): vector of parameters, with an index for parameters
            (including v2/v1 ratio)
    """
    all_param_names = (list(interps["params_with_cov"])
                        + list(interps["secondary_params"])
                        + ["v2v1_ratio"])
    ser_p = pd.Series(np.zeros(len(all_param_names)),
                index=pd.Index(all_param_names, name="Parameter"))
    mean_vec = []
    mean_interps = interps["ser_splines_means"]
    for pi in interps["params_with_cov"]:
        mean_vec.append(mean_interps.loc[pi](log10ec50))
    mean_vec = np.asarray(mean_vec)
    ser_p.loc[interps["params_with_cov"]] = mean_vec

    # Secondary parameters with a single vector-valued interpolation function
    other_p = interps["secondary_params"]
    ser_p.loc[other_p] = mean_interps.loc["secondary_params"](log10ec50)

    # v2/v1 ratio
    ser_p.loc["v2v1_ratio"] = interps["v2v1_stats"].loc["median"]
    return ser_p


def generate_LS(tpoints, log10ec50, interps,
                    add_noise=True, rgen=None, nsamp=1):
    # First, generate parameter values
    if add_noise:
        df_p = generate_sample_params(log10ec50, interps,
                                    rgen=rgen, nsamp=nsamp)
    elif nsamp > 1:
        raise ValueError("Asking for nsamp > 1 while add_noise=False "
                "would produce identical trajectories, which makes no sense.")
    else:
        df_p = generate_mean_params(log10ec50, interps).to_frame().T
        df_p.index.name = "Sample"

    # Second, generate trajectories for each sampled parameter
    # Make sure we have the right parameters.
    assert np.all(np.asarray(["a0", "tau0", "theta", "v1",
                        "alpha", "beta", "v2v1_ratio"]) == df_p.columns.values)
    # Normalize time
    tscale = interps["tscale"]
    taus = tpoints / tscale
    # Initialize DataFrame
    idx = pd.MultiIndex.from_product([df_p.index.values, tpoints],
                        names=["Sample", "Time"])
    cols = pd.Index(["LS1", "LS2"], name="Feature")
    df_traj = pd.DataFrame(np.zeros([len(idx), len(cols)]),
                        index=idx, columns=cols)
    # Compute trajectories for each parameter set
    for key in df_p.index:
        params = df_p.loc[key].values
        LS12 = ballistic_sigmoid_freealpha(taus, *params)
        df_traj.loc[key, "LS1":"LS2"] = LS12.T
    # Remove the Sample level if there is only one
    if nsamp == 1:
        df_traj = df_traj.xs(0, level="Sample")
    return df_traj


def generate_ls_conc(tpoints, log10ec50, interps,
                    add_noise=True, rgen=None, nsamp=1):
    # First, generate parameter values
    if add_noise:
        df_p = generate_sample_params(log10ec50, interps,
                                    rgen=rgen, nsamp=nsamp)
    elif nsamp > 1:
        raise ValueError("Asking for nsamp > 1 while add_noise=False "
                "would produce identical trajectories, which makes no sense.")
    else:
        df_p = generate_mean_params(log10ec50, interps).to_frame().T
        df_p.index.name = "Sample"

    # Second, generate trajectories for each sampled parameter
    # Make sure we have the right parameters.
    assert np.all(np.asarray(["a0", "tau0", "theta", "v1",
                        "alpha", "beta", "v2v1_ratio"]) == df_p.columns.values)
    # Normalize time
    tscale = interps["tscale"]
    taus = tpoints / tscale
    # Initialize DataFrame
    idx = pd.MultiIndex.from_product([df_p.index.values, tpoints],
                        names=["Sample", "Time"])
    cols = pd.Index(["ls1", "ls2"], name="Feature")
    df_traj = pd.DataFrame(np.zeros([len(idx), len(cols)]),
                        index=idx, columns=cols)
    # Compute trajectories for each parameter set
    for key in df_p.index:
        params = df_p.loc[key]
        ls12 = sigmoid_conc_full_freealpha(taus, params[:"beta"].values,
                        v2v1_ratio=params["v2v1_ratio"])
        # Normalize n1 and n2 by tscale, due to derivative wrt tau from N1, n2
        df_traj.loc[key, "ls1":"ls2"] = ls12.T / tscale
    # Remove the Sample level if there is only one
    if nsamp == 1:
        df_traj = df_traj.xs(0, level="Sample")
    return df_traj
