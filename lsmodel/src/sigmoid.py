import numpy as np
from scipy.special import hyp2f1


# The integral I(\tau, \tau_0, \gamma) = \int \frac{- e^{-\tau}}{e^{\gamma(\tau - \tau_0)} + 1}
# general and special gamma cases covered.
def sig_int(tau, tau_0, gamma):
    """ Integral of -e^{-\tau} / (1 + e^{\gamma(\tau - \tau_0)})
    """
    # Verification: is gamma a special case where the hypergeometric function fails?
    if abs(int(1/gamma) - 1/gamma) < 1e-12 and abs(int(1/gamma)) > 0.01:  # Not the 1/gamma = 0 case.
        print("We hit the special case 1/gamma = {}".format(1/gamma))
        n = int(1/gamma)
        res = np.exp(-tau)
        res += (-1)**n * n * np.exp(-tau_0) * np.log(np.exp(-gamma * tau) + np.exp(-gamma * tau_0))
        # Simpler to code a Python loop than to create a 2d array with one row per term in the sum...
        # If performance really becomes an issue, create arrays. But will probably never use this.
        for j in range(1, n):  # from 1 to n-1 included
            res += (-1)**j * n / (n-j) * np.exp(-j * tau_0 / n) * np.exp((j/n - 1) * tau)

    # Otherwise, can rely on 2F1
    else:
        res = np.exp(-tau) * hyp2f1(1, -1/gamma, 1 - 1/gamma, -np.exp(gamma*(tau - tau_0)))

    return res


## Improved ballistic model with a sigmoid and free alpha (more parameters)
# Full  model for sigmoid concentration n1 and n2 given just above.
def sigmoid_conc_full_freealpha(t, params, v2v1_ratio=1.):
    """ Argument t can be an array, other parameters are scalars. params is a list. """
    # Extract parameters
    a0, tau0, theta, v1, alpha, beta = params
    a1, a2 = a0 * np.cos(theta), a0 * np.sin(theta)  # theta in radians.
    tau = t * alpha
    v2 = v2v1_ratio * v1
    gamma = beta / alpha

    # Vector where 1st dimension is n1, second is n2
    r = np.zeros((2, t.shape[0]))

    # Common terms
    bound_exp = 1 - np.exp(-tau)
    exp_beta = np.exp(gamma*(tau - tau0)) + 1

    # Node 1
    r[0] = bound_exp * ((a1 + v1)/exp_beta - v1)

    # Node 2
    r[1] = (a2 + v2) * np.power(bound_exp, 2) / exp_beta - bound_exp * v2

    return r


# The main function computing [N_1, N_2]
def ballistic_sigmoid_freealpha(times, a0, tau0, theta, v1, alpha, beta, v2v1_ratio=1.):
    """ Integral expression for N_1, N_2, based on the first-order
    ballistic expression with a sigmoid for n_1 and n_2.

    Args:
        times (1darray): timepoints at which to compute the ballistic trajectories. Vector is doubled to accommodate fitting procedure
        a0 (float): initial acceleration
        tau0 (float): time of half-maximum sigmoidal decay, times alpha
        theta (float): angle of initial velocity (radians)
        v1 (float): terminal concentration in node 1
        alpha (float): production time scale
        beta (float): degradation time scale
        fit (bool): if True, the function is called for fitting.
            If false, don't double the return vector.
        upbounds (list): upper bound on the value of each parameter,
            for normalization in the regularization.
        lambd (float): weight to give to the regularization term.
    """
    # Initialize some variables
    a1, a2 = a0 * np.cos(theta), a0 * np.sin(theta)
    v2 = v1 * v2v1_ratio
    tau = times * alpha
    gamma = beta / alpha

    # Some terms used in both node 1 and 2
    lnterm_0 = np.log(1 + np.exp(-gamma*tau0)) / gamma
    lnterm = np.log(np.exp(-gamma*tau) + np.exp(-gamma*tau0)) / gamma
    boundedint = tau + np.exp(-tau)
    siginterm = sig_int(tau, tau0, gamma)

    # Constants to ensure it's zero at the origin
    k1 = v1 / alpha - (a1 + v1) / alpha * (sig_int(0, tau0, gamma) - lnterm_0)
    k2 = v2 / alpha + (a2 + v2) / alpha * (sig_int(0, 2*tau0, gamma/2)/2 - 2*sig_int(0, tau0, gamma) + lnterm_0)

    # Assemble terms
    N1 = (a1 + v1) / alpha * (siginterm - lnterm) - v1 / alpha * boundedint + k1
    N2 = (a2 + v2) / alpha * (2*siginterm - sig_int(2*tau, 2*tau0, gamma/2)/2 - lnterm)
    N2 += -v2 / alpha * boundedint + k2

    return np.array([N1, N2])
