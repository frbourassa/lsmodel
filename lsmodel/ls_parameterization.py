"""
Module with main user functions to estimate and interpolate LS model parameter
distributions.

@author: frbourassa
May 5, 2023
"""
import numpy as np
import pandas as pd
import json
import os

from lsmodel.src.distrib_interpolation import (
    stats_per_levels,
    interpolate_params_vs_logec50,
    linear_interpolate_params_vs_logec50,
    compute_cholesky_dataframe
)


# NB: to make this package even easier to use, we could estimate and interpo-
# late parameter distributions at import time of ls_generation functions

def load_param_fits(for_means=None, for_covs=None, for_v2v1=None, root=""):
    """ Load two param fits files, one for mean parameter estimation
    (fits on training datasets by default),
    and the other for covariance parameter estimation
    (fits on HighMI_3 dataset by default)

    Args:
        for_means (str): path to parameter fits file used to estimate
            the mean value of model parameters for each experimental peptide.
        for_covs (str): path to parameter fits file used to estimate
            the covariance of model parameters for each experimental peptide.
        for_v2v1 (str): path to v2/v1 slope fits file used to estimate
            the v2/v1 distribution
        root (str): path to lsmodel folder relative to current
            working directory

    Returns:
        (dict) containing four keys:
            for_means (pd.DataFrame): parameter fits to estimate means
            for_covs (pd.DataFrame): parameter fits to estimate covariances
            for_v2v1 (float): v2/v1 fits on various datasets
            tscale (float): factor by which time is rescaled
    """
    print("Starting to load parameter fits...")
    if for_means is None:
        for_means = os.path.join(root, "lsmodel", "params",
                    "df_params_Sigmoid_freealpha_reg04_selectdata.hdf")
    if for_covs is None:
        for_covs = os.path.join(root, "lsmodel", "params",
                    "df_params_Sigmoid_freealpha_HighMI_3.hdf")
    if for_v2v1 is None:
        for_v2v1 = os.path.join(root, "lsmodel", "params",
                    "df_v2v1_Sigmoid_freealpha_reg04_selectdata.hdf")
    for_tscale = os.path.join(root, "lsmodel", "params", "timescale.json")

    pfits_for_means = pd.read_hdf(for_means)
    pfits_for_covs = pd.read_hdf(for_covs)
    pfits_for_v2v1 = pd.read_hdf(for_v2v1)
    with open(for_tscale, "r") as h:
        tscale = json.load(h).get("timescale")

    print("Finished loading parameter fits!")
    return {"for_means": pfits_for_means, "for_covs": pfits_for_covs,
            "for_v2v1": pfits_for_v2v1, "tscale":tscale}


def interpolate_param_distrib(paramfits, **kwargs):
    """
    Args:
        paramfits (dict): as returned by lsmodel.ls_generation.load_param_fits

    Keyword args:
        ec50file (str): path to file with EC50 of tested peptides.
        tcn_fit_mean (str): T cell number to consider for means, or 'all'
        tcn_fit_cov (str): T cell number to consider for covariances, or 'all'
        params_with_cov (list of str): for which parameters
                we fit a covariance matrix. Default: ["a0", "tau0", "theta"]
        all_param_names (list of str): name of all parameters expected
            by the model.
            Default: ["a0", "tau0", "theta", "v1", "alpha", "beta"]
        levels_group (list of str): which level(s) the parameters are grouped
            by before estimating distributions. Default: ["Peptide"]
    Returns:

    """
    print("Starting to estimate and interpolate parameter distributions...")
    # Load EC50 file and compute average log10 EC50
    root = kwargs.get("root", "")
    ec50file = kwargs.get("ec50file",
            os.path.join(root, "lsmodel", "params", "potencies_df_2021.json"))
    df_ec50s_refs = pd.read_json(ec50file)
    df_ec50s_refs.columns.name = "Reference"
    df_ec50s_refs.index.name = "Peptide"
    ser_ec50s_avglog = np.log10(df_ec50s_refs).mean(axis=1)

    # Read other kwargs
    tcn_fit_cov = kwargs.get("tcn_fit_cov", "30k")
    tcn_fit_mean = kwargs.get("tcn_fit_mean", "100k")
    params_with_cov = kwargs.get("params_with_cov", ["a0", "tau0", "theta"])
    all_param_names = kwargs.get("all_param_names",
                        ["a0", "tau0", "theta", "v1", "alpha", "beta"])
    levels_group = kwargs.get("levels_group", ["Peptide"])

    ## 1. Estimate and interpolate the mean of parameters with covariance
    pfits_for_means = paramfits.get("for_means")
    try:
        pfits_for_means = pfits_for_means.xs(tcn_fit_mean,
                        level="TCellNumber", axis=0, drop_level=True)
    except KeyError:
        print("TCellNumber was already sliced; skipping.")

    # Drop A8 and Q7, which we don't use in general.
    pfits_for_means = pfits_for_means.drop(["A8", "Q7"], level="Peptide")

    # Only keep G4 and E1 if theta < 0; we know that others are outliers,
    # artifacts due to insufficient regularization or background fluorescence
    pfits_for_means = pfits_for_means.loc[np.logical_not(
            pfits_for_means.index.isin(["E1", "G4"], level="Peptide")
            & (pfits_for_means["theta"] > 0.0))]

    ret = stats_per_levels(pfits_for_means, levels_groupby=levels_group,
                            feats_keep=params_with_cov)
    # Drop covariances, these are estimated elsewhere.
    df_p_means, df_p_means_estim_vari, _, _, _ = ret

    # We might have zero variance on E1/G4 which are at zero
    # This can cause bugs when interpolating with zero error bars,
    # so set variance to very small value
    df_p_means_estim_vari = df_p_means_estim_vari.clip(
                                lower=np.abs(df_p_means).min().min())

    ser_splines_means = interpolate_params_vs_logec50(df_p_means,
            df_p_means_estim_vari, ser_ec50s_avglog, x_name="Peptide")


    ## 2. Estimate and interpolate covariance matrix of some parameters
    pfits_for_covs = paramfits.get("for_covs")

    # Select a T cell number or marginalize over all. In HighMI_3, only 30k.
    if tcn_fit_cov == "all":
        ret = stats_per_levels(pfits_for_covs,
                    levels_groupby=levels_group, feats_keep=params_with_cov)
    else:
        ret = stats_per_levels(pfits_for_covs
                    .xs(tcn_fit_cov, level="TCellNumber", axis=0),
                    levels_groupby=levels_group, feats_keep=params_with_cov)
    # Disregard the means from that dataset
    (_, _, df_params_covs, df_params_covs_estim_vari, ser_npts) = ret
    df_params_covs_estim_vari = np.abs(df_params_covs_estim_vari)

    # Interpolate estimators of the covariance matrix's Cholesky decomposition
    # to ensure the interpolated matrices are still positive definite
    ret = compute_cholesky_dataframe(df_params_covs, ser_npts)
    df_params_chol, df_params_chol_estim_vari = ret

    ser_splines_chol = interpolate_params_vs_logec50(df_params_chol,
            df_params_chol_estim_vari, ser_ec50s_avglog, x_name="Peptide")
    # Also directly interpolate covariance matrices for comparison
    ser_splines_covs = interpolate_params_vs_logec50(df_params_covs,
            df_params_covs_estim_vari, ser_ec50s_avglog, x_name="Peptide")

    ## 3. For secondary parameters, just linearly interpolate their separate
    # means and neglect their sloppy, scattered variances.
    secondary_params = all_param_names.copy()
    for p in params_with_cov:
        try: secondary_params.remove(p)
        except ValueError: continue

    ret = stats_per_levels(pfits_for_means, levels_groupby=levels_group,
                            feats_keep=secondary_params)
    # Drop estimator variances, the distributions are so ill-conditioned
    # we will just interpolate them linearly without smoothing
    df_p_others, _, df_p_cov_others, _, _ = ret

    # Keep only the variance, not covariance of these sloppy parameters
    varkeys = [a+"*"+a for a in secondary_params]
    df_p_var_others = df_p_cov_others.loc[:, varkeys]

    # Interpolate with linear interpolation method, no smoothing
    secondary_params_means_interp1d = linear_interpolate_params_vs_logec50(
                        df_p_others, ser_ec50s_avglog, x_name="Peptide")
    secondary_params_vars_interp1d = linear_interpolate_params_vs_logec50(
                        df_p_var_others, ser_ec50s_avglog, x_name="Peptide")
    # Add these interpolation functions to the series of means and variances
    ser_splines_means["secondary_params"] = secondary_params_means_interp1d
    ser_splines_covs["secondary_params"] = secondary_params_vars_interp1d

    ## 4. Compute v2/v1 median and median absolute deviation (MAD). This ratio
    # will be sampled from a (clipped) Cauchy distribution with that MAD.
    # This is not an interpolation: easier to deal with!
    pfits_for_v2v1 = paramfits["for_v2v1"]
    v2v1_median = pfits_for_v2v1.median()
    # .mad in pandas is mean absolute deviation, here we want median AD
    v2v1_mad = abs(pfits_for_v2v1 - pfits_for_v2v1.median()).median()
    v2v1_stats = pd.Series([v2v1_median, v2v1_mad],
                index=pd.Index(["median", "MAD"], name="statistics"),
                name="v2v1_stats")

    # Build a returns dictionary to be used by generate_LS, generate_ls_conc
    # so the user has to deal with a single package of input parameters.
    retdict = {
        "ser_splines_means": ser_splines_means,
        "ser_splines_chol": ser_splines_chol,
        "ser_splines_covs": ser_splines_covs,
        "v2v1_stats": v2v1_stats,
        "tscale":paramfits.get("tscale"),
        "params_with_cov": params_with_cov,
        "secondary_params": secondary_params,
        "log10ec50s": ser_ec50s_avglog
    }
    print("Finished estimating and interpolating parameter distributions!")
    return retdict
